# Automation Tests

Place holder for Automation Framework test

## Flow for pulling and pushing Automation code
1. Make the fork of the main repo.
2. Clone the "forked" repo to local dev box.
3. Make the connection to the main repo using the command ` git remote add <name for reference> <git url of main repo`.
Once the above gets completed successfully. Verify it using the command ` git remote -v`.
4. Modify/create automation code on local dev box.
5. Push the code to fork using the command ` git push origin <branch name>`
6. Create MR(Merge request) from "forked" repo to main repo (As caution always pull from main repo to forked repo before MR to resolve conflicts if any in the code. `git pull <name for reference used while creating fork> <branch name>` )
