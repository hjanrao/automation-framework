package test.automation.framework.utils;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonReaderFactory;
import javax.json.JsonValue;

import org.apache.log4j.Logger;

import com.google.common.io.Resources;

public class TestHelper 
{
	private static Logger log = Logger.getLogger(TestHelper.class);


	/**
	 * Method to load file to JsonObject
	 * @param recourseName
	 * @return
	 */
	public static JsonObject loadJsonFile(String recourseName )
	{
		JsonObject jsonObject = null;
		URL url = Resources.getResource(recourseName);
		JsonReaderFactory factory = Json.createReaderFactory(null);
		try (JsonReader jsonReader = factory.createReader(url.openStream())){
			jsonObject = jsonReader.readObject();
		} catch (IOException e) {
			log.warn("Error copying the file", e);;
			throw new RuntimeException("Was not able to read file " + recourseName, e);
		}
		return jsonObject;

	}



	/**
	 * Method to convert string to JsonObject
	 * @param jsonObjectStr
	 * @return
	 */
	public static JsonObject jsonFromString(String jsonObjectStr) 
	{
		JsonReader jsonReader = Json.createReader(new StringReader(jsonObjectStr));
		JsonObject object = jsonReader.readObject();
		jsonReader.close();
		return object;
	}


	/**
	 * Method to remove property from JsonObject
	 * @param original
	 * @param key
	 * @return
	 */
	public static JsonObject removeProperty(JsonObject original, String key){
		JsonObjectBuilder builder = Json.createObjectBuilder();

		for (Map.Entry<String,JsonValue> entry : original.entrySet()){
			if (entry.getKey().equals(key)){
				continue;
			} else {
				builder.add(entry.getKey(), entry.getValue());
			}
		}       
		return builder.build();
	}
}
