package test.automation.framework.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.log4testng.Logger;

import test.automation.framework.ui.utils.Browser;

public class PropertyUtils {

	private static Logger log = Logger.getLogger(PropertyUtils.class);
	private static final String PROPERTY_FILE_NAME = "config.properties";
	private static final Properties PROPERTIES = getProperties();

	/**
	 * Method to get the property
	 * @return
	 */
	private static synchronized Properties getProperties()
	{
		try
		{
			InputStream inputStream = new BufferedInputStream(new FileInputStream(PROPERTY_FILE_NAME));
			Properties properties = new Properties();
			properties.load(inputStream);
			return properties;
		}
		catch(IOException e)
		{

			log.warn("Error reading the properties file", e);
			throw new RuntimeException("Error loading the properties file "+e);
		}
	}

	/**
	 * Method to get the server
	 * @return
	 */
	public static String getServer()
	{
		return System.getProperty("server", PROPERTIES.getProperty("server"));
	}


	/**
	 * Method to get the port
	 * @return
	 */
	public static int getPort()
	{
		return Integer.parseInt(PROPERTIES.getProperty("port"));
	}


	/**
	 * Method to get the protocol
	 * @return
	 */
	public static String getProtocol()
	{
		return System.getProperty("protocol", PROPERTIES.getProperty("protocol"));
	}


	/**
	 * 
	 * @return
	 */
	public static Browser getBrowser()
	{
		return Browser.valueOf((System.getProperty("browser", PROPERTIES.getProperty("browser")).toUpperCase()));
	}
	
	/**
	 * 
	 * @return
	 */
	
	
	/**
	 * 
	 * @return
	 */
	public static String downloadmobileclient()
	{
		return System.getProperty("downloadmobileclient", PROPERTIES.getProperty("downloadmobileclient"));
	}



	/**
	 * Method to get the apps url
	 * @return
	 */
	public static String getsAppUrl()
	{
		return System.getProperty("appsUrl", PROPERTIES.getProperty("appsUrl"));
	}



	/**
	 * Method to check if selenium enabled or not
	 * @return
	 */
	public static boolean isSeleniumGridEnabled() {
		return Boolean.valueOf(System.getProperty("isGridEnabled", PROPERTIES.getProperty("isGridEnabled")));
	}


	/**
	 * Method to get the selenium hub url
	 * @return
	 */
	public static String getSeleniumGridHubUrl() {
		String seleniumHubUrl = System.getProperty("seleniumHub", PROPERTIES.getProperty("seleniumHub"));
		return seleniumHubUrl;
	}


	/**
	 * Method to get the default timeout set via config
	 * @return
	 */
	public static long getDefaultTimeOutForElement()
	{
		long timeout = Long.parseLong(System.getProperty("defaultTimeoutForElement", PROPERTIES.getProperty("defaultTimeoutForElement")));
		return timeout;
	}


	/**
	 * Method to get the default data directory to look for test data
	 * @return
	 */
	public static String getDataDir()
	{
		return System.getProperty("dataDir", PROPERTIES.getProperty("dataDir"));
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static String isReportingEnabled()
	{
		return System.getProperty("saveReport", PROPERTIES.getProperty("saveReport"));
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static String getReportingServer()
	{
		return System.getProperty("reportServer", PROPERTIES.getProperty("reportServer"));
	}
	
	
	/**
	 * Method to get the default data directory to look for test data
	 * @return
	 */
	public static String getReportingPort()
	{
		return System.getProperty("reportServerPort", PROPERTIES.getProperty("reportServerPort"));
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static String getReportingAdmin()
	{
		return System.getProperty("reportAdminUser", PROPERTIES.getProperty("reportAdminUser"));
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static String getReportingPasssword()
	{
		return System.getProperty("reportAdminPassword", PROPERTIES.getProperty("reportAdminPassword"));
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public static String isRealTimeReportingEnabled()
	{
		return System.getProperty("saveRealTimeReport", PROPERTIES.getProperty("saveRealTimeReport"));
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static String getRealTimeReportingServer()
	{
		return System.getProperty("realTimeReportServer", PROPERTIES.getProperty("realTimeReportServer"));
	}
	
	
	/**
	 * Method to get the default data directory to look for test data
	 * @return
	 */
	public static String getRealTimeReportingPort()
	{
		return System.getProperty("realTimeReportServerPort", PROPERTIES.getProperty("reportServerPort"));
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static String getRealTimeReportingAdmin()
	{
		return System.getProperty("realTimeReportAdminUser", PROPERTIES.getProperty("reportAdminUser"));
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static String getRealTimeReportingPasssword()
	{
		return System.getProperty("realTimeReportAdminPassword", PROPERTIES.getProperty("reportAdminPassword"));
	}

	/**
	 *
	 * @return
	 */
	public static String getRealTimeReportingDatabase()
	{
		return System.getProperty("realTimeReportDatabase", PROPERTIES.getProperty("realTimeReportDatabase"));
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getbuild()
	{
		return System.getProperty("build", PROPERTIES.getProperty("build"));
	}

	/**
	 *
	 * @return username
	 */
	public static String getSupplierUserName()
	{
		return System.getProperty("supplierUserName", PROPERTIES.getProperty("supplier.username"));
	}


	/**
	 *
	 * @return password
	 */
	public static String getSupplierPassword()
	{
		return System.getProperty("supplierPassword", PROPERTIES.getProperty("supplier.password"));
	}
}
