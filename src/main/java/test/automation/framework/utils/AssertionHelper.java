package test.automation.framework.utils;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.testng.Assert;

public class AssertionHelper 
{
	private static Logger log = Logger.getLogger(AssertionHelper.class);




	/**
	 * Method for asserting the status code
	 * @param res
	 * @param expectedStatus
	 */
	public static void assertStatusCode(Response res, int expectedStatus,String message)
	{
		Assert.assertEquals(res.getStatus(), expectedStatus,message);
	}


	/**
	 * Method for asserting the status code
	 * @param res
	 * @param expectedStatus
	 */
	public static void assertStatusCode(Response res, int expectedStatus)
	{
		Assert.assertEquals(res.getStatus(), expectedStatus,"Status code is not as expected");
	}


}
