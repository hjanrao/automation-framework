package test.automation.framework.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import org.apache.log4j.Logger;

import com.google.common.io.Files;
import com.google.common.io.Resources;

public class FileUtils 
{

	private static Logger log = Logger.getLogger(FileUtils.class);


	/**
	 * Method to copy file from resources to tmp folder
	 * @param recourseName
	 * @return
	 */
	public static String copyRecourseToTemporaryFolder(String recourseName) {

		File directory = Files.createTempDir();
		File file = new File(directory, recourseName);
		URL url = Resources.getResource(recourseName);
		try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
			Resources.copy(url, fileOutputStream);
		} catch (IOException e) {
			log.warn("Error copying the file", e);;
			throw new RuntimeException("Was not able to read file " + file, e);
		}

		return file.getPath();
	}
}
