package test.automation.framework.utils;

public class ResponseMessage 
{

	private int errorCode;
	private String errorMessage;
	private String warningMessage;
	private String otherStatusMessages;
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getWarningMessage() {
		return warningMessage;
	}
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
	public String getOtherStatusMessages() {
		return otherStatusMessages;
	}
	public void setOtherStatusMessages(String otherStatusMessages) {
		this.otherStatusMessages = otherStatusMessages;
	}
	
	
}
