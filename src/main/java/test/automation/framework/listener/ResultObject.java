package test.automation.framework.listener;

public class ResultObject 
{

	private String testcaseid;
	private String testStatus;
	private String testTicketNo;
	private String failException;
	private String testCaseGroup;
	private String buildID;
	private String testClass;
	private String testMethod;
	private String failExceptionStackTrace;
	private double testDurationInSeconds;
	private String testExecutionDateTime;
	
	public String getTestExecutionDateTime() {
		return testExecutionDateTime;
	}
	public void setTestExecutionDateTime(String testExecutionDateTime) {
		this.testExecutionDateTime = testExecutionDateTime;
	}
	public double getTestDurationInSeconds() {
		return testDurationInSeconds;
	}
	public void setTestDurationInSeconds(double testDurationInSeconds) {
		this.testDurationInSeconds = testDurationInSeconds;
	}
	public String getFailExceptionStackTrace() {
		return failExceptionStackTrace;
	}
	public void setFailExceptionStackTrace(String failExceptionStackTrace) {
		this.failExceptionStackTrace = failExceptionStackTrace;
	}
	public String getTestClass() {
		return testClass;
	}
	public void setTestClass(String testClass) {
		this.testClass = testClass;
	}
	public String getTestMethod() {
		return testMethod;
	}
	public void setTestMethod(String testMethod) {
		this.testMethod = testMethod;
	}
	public String getBuildID() {
		return buildID;
	}
	public void setBuildID(String buildID) {
		this.buildID = buildID;
	}
	public String getTestcaseid() {
		return testcaseid;
	}
	public void setTestcaseid(String testcaseid) {
		this.testcaseid = testcaseid;
	}
	public String getTestStatus() {
		return testStatus;
	}
	public void setTestStatus(String testStatus) {
		this.testStatus = testStatus;
	}
	public String getTestTicketNo() {
		return testTicketNo;
	}
	public void setTestTicketNo(String testTicketNo) {
		this.testTicketNo = testTicketNo;
	}
	public String getFailException() {
		return failException;
	}
	public void setFailException(String failException) {
		this.failException = failException;
	}
	public String getTestCaseGroup() {
		return testCaseGroup;
	}
	public void setTestCaseGroup(String testCaseGroup) {
		this.testCaseGroup = testCaseGroup;
	}
	
}
