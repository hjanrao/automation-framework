package test.automation.framework.listener;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.testng.IAnnotationTransformer2;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.TestListenerAdapter;
import org.testng.annotations.IConfigurationAnnotation;
import org.testng.annotations.IDataProviderAnnotation;
import org.testng.annotations.IFactoryAnnotation;
import org.testng.annotations.ITestAnnotation;
import org.testng.annotations.Test;
import org.testng.internal.annotations.ConfigurationAnnotation;

import test.automation.framework.api.annotations.Groups;
import test.automation.framework.api.annotations.TestID;
import test.automation.framework.utils.PropertyUtils;

public class TestReportingListener extends TestListenerAdapter implements IAnnotationTransformer2
{

	protected Logger log = Logger.getLogger(getClass());
	private boolean isReportingEnabled = Boolean.valueOf(PropertyUtils.isReportingEnabled());
	private int reportingServerPort = Integer.parseInt(PropertyUtils.getReportingPort());
	private String reportingServer = PropertyUtils.getReportingServer();
	private String reportingAdmin = PropertyUtils.getReportingAdmin();
	private String reportingPassword = PropertyUtils.getReportingPasssword();
	
	private boolean isRealTimeReportingEnabled = Boolean.valueOf(PropertyUtils.isRealTimeReportingEnabled());
	private int realTimeReportingServerPort = Integer.parseInt(PropertyUtils.getRealTimeReportingPort());
	private String realTimeReportingServer = PropertyUtils.getRealTimeReportingServer();
	private String realTimeReportingAdmin = PropertyUtils.getRealTimeReportingAdmin();
	private String realTimeReportingPassword = PropertyUtils.getRealTimeReportingPasssword();
	private String build = PropertyUtils.getbuild();
	private ResultObject testResult = null;

	private InfluxDB INFLUXDB = null;
	private String DATABASE = PropertyUtils.getRealTimeReportingDatabase();


	/**
	 * Test passed
	 */
	private static final int RESULT_PASS = 0;
	/**
	 * Test failed because of assertion or exception
	 */
	private static final int RESULT_FAIL_TEST = 1;
	/**
	 * Test failed because setup method failed
	 */
	private static final int RESULT_FAIL_CONFIG = 2;
	/**
	 * Test skipped because it threw SkipException
	 */
	private static final int RESULT_SKIP_EXCEPTION = 4;
	/**
	 * Test skipped because a dependent test failed
	 */
	private static final int RESULT_SKIP_DEPENDENT = 5;
	/**
	 * Test skipped for an unknown reason
	 */
	private static final int RESULT_SKIP_UNKNOWN = 6;

	private static final int RESULT_SKIP_CONFIG = 7;


	private static final String TEST_SKIPPED = "SKIPPED";
	private static final String TEST_PASSED = "PASSED";
	private static final String TEST_FAILED = "FAILED";

	private void sendDataToInfluxDB(Point point)
	{
		INFLUXDB.write(point);
	}

	/**
	 * 
	 * @param actualTR
	 * @param result
	 * @param otherTR
	 */
	private void reportResult(ITestResult actualTR,int result, ITestResult otherTR)
	{
		boolean hasTestCaseID = actualTR.getMethod().getConstructorOrMethod().getMethod().isAnnotationPresent(TestID.class);
		if(reportingServer!=null && hasTestCaseID)
		{
			// Test Case class and name
			Class<?> testClass = actualTR.getTestClass().getRealClass();
			Method realMethod = actualTR.getMethod().getConstructorOrMethod().getMethod();

			// find the index if this is an "enumerated" test
			int invocationCount = -1;
			if (actualTR.getMethod().getParameterInvocationCount() > 1 ||
					realMethod.getAnnotation(Test.class).dataProvider().length() > 0)
				invocationCount = actualTR.getMethod().getCurrentInvocationCount();

			// Result
			if (result == RESULT_SKIP_EXCEPTION || result == RESULT_SKIP_DEPENDENT || result == RESULT_SKIP_UNKNOWN || result == RESULT_SKIP_CONFIG)
			{
				testResult.setTestStatus(TEST_SKIPPED);
			}
			else if (result == RESULT_PASS)
			{
				testResult.setTestStatus(TEST_PASSED);
			}
			else if (result == RESULT_FAIL_TEST || result == RESULT_FAIL_CONFIG)
			{
				testResult.setTestStatus(TEST_FAILED);
			}

			// Test_Type
			String category = null;
			for (String group : actualTR.getMethod().getGroups()) 
			{
				// search groups to find category
				switch (group) {
				case Groups.CATEGORY_SANITY:
					category = "Sanity";
					break;
				case Groups.CATEGORY_CONFIDENCE:
					category = "Confidence";
					break;
				case Groups.CATEGORY_API:
					category = "Api";
					break;
				case Groups.CATEGORY_UI:
					category = "UI";
					break;
				}
			}

			StringBuilder errorMessage = new StringBuilder();
			StringBuilder errorException = new StringBuilder();

			testResult.setTestCaseGroup(category);

			if (result == RESULT_FAIL_TEST) {

				// add e	xception to short message
				errorMessage.append(actualTR.getThrowable().getMessage());
				// add stack trace to long message
				errorException.append(getDetailedStackTrace(actualTR.getThrowable()));
			} else if (result == RESULT_FAIL_CONFIG) {
				// add text that a configuration method failed to short message
				errorMessage.append("Error occured in configuration method ");
				errorMessage.append(otherTR.getMethod().getMethodName());
				errorMessage.append(". ").append(otherTR.getThrowable().getMessage());
				// add stack trace to long message
				errorException.append(getDetailedStackTrace(otherTR.getThrowable()));
			} else if (result == RESULT_SKIP_EXCEPTION) {
				// add text that SkipException was thrown to short message
				errorMessage.append("Test is disabled with SkipException. ");
				errorMessage.append(actualTR.getThrowable().getMessage());
			} else if (result == RESULT_SKIP_DEPENDENT) {
				// add text that a dependent test failed to short message
				errorMessage.append("Test was skipped because dependent method ");
				errorMessage.append(otherTR.getMethod().getMethodName());
				errorMessage.append(" failed. ");
			} else if (result == RESULT_SKIP_CONFIG) {
				// add text that a configuration method was skipped to short message
				errorMessage.append("Test was skipped because a configuration method was skipped. ");
				if (otherTR.getThrowable() != null)
					errorMessage.append(otherTR.getThrowable().getMessage());
				else
					errorMessage.append("(no Throwable was found)");
			}

			if(errorMessage.length()>0)
			{
				testResult.setFailException(errorMessage.toString());
			}

			if(errorException.length()>0)
			{
				testResult.setFailException(errorException.toString());
			}

			long durationMills = actualTR.getEndMillis() - actualTR.getStartMillis();
			double durationSeconds = durationMills / (double) 1000;
			testResult.setTestDurationInSeconds(durationSeconds);

			// Executed_At
			DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG);
			String executed = formatter.format(new Date(actualTR.getStartMillis()));
			testResult.setTestExecutionDateTime(executed);
			testResult.setTestClass(actualTR.getTestClass().getName());
			testResult.setTestMethod(actualTR.getMethod().getMethodName());
			testResult.setTestcaseid(actualTR.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestID.class).value());
		}

		else if (!hasTestCaseID) {
			// warn the user if there was no GUID
			log.warn("This testcase does not have a GUID: " + actualTR.getMethod().toString());
		}

		if(isRealTimeReportingEnabled && realTimeReportingServer!=null)
		{
			sendTestMethodStatus(testResult);
		}

	}

	@Override
	public void onStart(ITestContext context)
	{
		super.onStart(context);
		INFLUXDB= InfluxDBFactory.connect(PropertyUtils.getRealTimeReportingServer());

		testResult = new ResultObject();
		testResult.setBuildID(build);
		INFLUXDB.setDatabase(DATABASE);
	}

	@Override
	public void onFinish(ITestContext context)
	{
		super.onFinish(context);
	}


	@Override
	public void onTestSuccess(ITestResult actualTR)
	{
		super.onTestSuccess(actualTR);
		reportResult(actualTR, RESULT_PASS, null);


	}

	@Override
	public void onTestFailure(ITestResult actualTR)
	{
		super.onTestFailure(actualTR);
		reportResult(actualTR, RESULT_FAIL_TEST, null);
	}

	@Override
	public void onTestSkipped(ITestResult tr)
	{
		super.onTestSkipped(tr);

		ITestResult otherTR = null;
		int status = RESULT_SKIP_UNKNOWN;

		if(isSkipException(tr))
			status = RESULT_SKIP_EXCEPTION;
		else if((otherTR = getConfigurationFailure(tr)) != null)
			status = RESULT_FAIL_CONFIG;
		else if((otherTR = getConfigurationSkip(tr)) != null)
			status = RESULT_SKIP_CONFIG;
		else if((otherTR = getDependencyFailure(tr)) != null)
			status = RESULT_SKIP_DEPENDENT;
		reportResult(tr, status, otherTR);


	}


	/**
	 * 
	 * @param tr
	 * @return
	 */
	private ITestResult getDependencyFailure(ITestResult tr)
	{
		ITestResult dependTR = null;

		// get the list of dependent methods
		String[] dependUpon = tr.getMethod().getMethodsDependedUpon();
		// check if it's a hard dependency (skip test if dependent failed)
		boolean hardDependency = !tr.getMethod().isAlwaysRun();

		// if there are dependencies and it's a hard dependency, check for failures
		if(dependUpon != null && dependUpon.length > 0 && hardDependency)
		{
			// loop through failed tests
			Iterator<ITestResult> failedItr = this.getFailedTests().iterator();
			while(failedItr.hasNext() && dependTR == null)
			{
				ITestResult testFailure = failedItr.next();

				// make sure we're in the same class
				if(testFailure.getTestClass().equals(tr.getTestClass()))
				{
					String failedTestName = testFailure.getMethod().getRealClass().getName();
					failedTestName += "." + testFailure.getMethod().getMethodName();
					// search the list of dependencies for a matching method name
					for(int i = 0; i < dependUpon.length && dependTR == null; i++)
						if(dependUpon[i].equals(failedTestName))
							dependTR = testFailure;
				}
			}
		}

		return dependTR;
	}


	/**
	 * 
	 * @param tr
	 * @return
	 */
	private ITestResult getConfigurationFailure(ITestResult tr)
	{
		ITestResult configTR = null;

		Iterator<ITestResult> configItr = this.getConfigurationFailures().iterator();
		while(configItr.hasNext() && configTR == null)
		{
			ITestResult someTR = configItr.next();

			// check if the configuration result is in the same class,
			// a @BeforeClass method, and a @BeforeMethod method
			boolean isSameClass = someTR.getTestClass().equals(tr.getTestClass());
			boolean isBeforeClass = someTR.getMethod().isBeforeClassConfiguration();
			boolean isBeforeMethod = someTR.getMethod().isBeforeMethodConfiguration();

			if(isSameClass && (isBeforeClass || isBeforeMethod))
				configTR = someTR;
		}

		return configTR;
	}


	/**
	 * 
	 * @param tr
	 * @return
	 */
	private ITestResult getConfigurationSkip(ITestResult tr)
	{
		ITestResult configTR = null;

		Iterator<ITestResult> configItr = this.getConfigurationSkips().iterator();
		while(configItr.hasNext() && configTR == null)
		{
			ITestResult someTR = configItr.next();

			// check if the configuration result is in the same class,
			// a @BeforeClass method, and a @BeforeMethod method
			boolean isSameClass = someTR.getTestClass().equals(tr.getTestClass());
			boolean isBeforeClass = someTR.getMethod().isBeforeClassConfiguration();
			boolean isBeforeMethod = someTR.getMethod().isBeforeMethodConfiguration();

			if(isSameClass && (isBeforeClass || isBeforeMethod))
				configTR = someTR;
		}

		return configTR;
	}

	/**
	 * Checks the test result to see if the test was skipped because it threw
	 * the SkipException.
	 * 
	 * @param tr the test result
	 * @return true if SkipException was thrown, false otherwise
	 */
	private boolean isSkipException(ITestResult tr)
	{
		boolean result = false;

		Throwable throwable = tr.getThrowable();
		if(throwable != null)
			result = (throwable.getClass() == SkipException.class);

		return result;
	}


	/**
	 * 
	 * @param ex
	 * @return
	 */
	private String getDetailedStackTrace(Throwable ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	@Override
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
		// TODO Auto-generated method stub

	}

	@Override
	public void transform(IConfigurationAnnotation annotation, Class testClass, Constructor testConstructor,
			Method testMethod) {
		if(annotation instanceof ConfigurationAnnotation)
			((ConfigurationAnnotation) annotation).setAlwaysRun(true);

	}

	@Override
	public void transform(IDataProviderAnnotation annotation, Method method) {
		// TODO Auto-generated method stub

	}

	@Override
	public void transform(IFactoryAnnotation annotation, Method method) {
		// TODO Auto-generated method stub
	}

	private void sendTestMethodStatus(ResultObject result) {
		Point point = Point.measurement("testmethod")
				.time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
				.tag("testclass", result.getTestClass())
				.tag("name", result.getTestMethod())
				.tag("result", result.getTestStatus())
				.addField("duration", (result.getTestDurationInSeconds()))
				.addField("result", result.getTestStatus())
				.addField("name", result.getTestMethod())
				.build();
		sendDataToInfluxDB(point);
	}
}
