package test.automation.framework.ui.utils;

public enum Browser
{
	FIREFOX,
	GECKO,
	CHROME,
	SAUCELABS
}

