package test.automation.framework.ui.utils;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.internal.ElementScrollBehavior;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import test.automation.framework.utils.FileUtils;
import test.automation.framework.utils.PropertyUtils;

public class WebDriverManager
{
	private static Logger log = Logger.getLogger(WebDriverManager.class);

	public static final String USERNAME = "sso-wayfair-hjanrao";
	public static final String ACCESS_KEY = "9894f3ec-8521-42d4-952b-06ed5fd07e0a";
	public static final String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";

	public static WebDriver getWebDriver()
	{
		Browser browser = PropertyUtils.getBrowser();
		WebDriver driver = getWebdriver(browser);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.setProperty("webdriver.timeouts.implicitlywait", "1");

		return driver;
	}


	public  static WebDriver getWebdriver(Browser browser)
	{
		boolean isSeleniumGridEnabled = PropertyUtils.isSeleniumGridEnabled();
		log.info(String.format("Starting '%s' browser", browser));

		switch (browser) {
			case FIREFOX:
				DesiredCapabilities cap = getFireFoxDesiredCapabilities();
				cap.setCapability(FirefoxDriver.MARIONETTE, false);
				System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "false");
				try {
					if (isSeleniumGridEnabled) {
						log.info(String.format("Grid is enabled [%s]", getSeleniumGridHubUrl()));
						RemoteWebDriver remoteWebDriver = new RemoteWebDriver(getSeleniumGridHubUrl(), cap);
						remoteWebDriver.setFileDetector(new LocalFileDetector());
						return remoteWebDriver;
					} else {
						return new FirefoxDriver(cap);
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					log.warn("Error creating driver", e);
					throw new RuntimeException("Failed to get the driver", e);
				}
			case GECKO:
				String geckoDriverloc = null;
				if (System.getProperty("os.name").contains("Windows")) {
					log.info("Client Machine is windows");
					geckoDriverloc = FileUtils.copyRecourseToTemporaryFolder("geckodriver.exe");
				} else {
					log.info("Machine is not windows");
					geckoDriverloc = FileUtils.copyRecourseToTemporaryFolder("geckodriver");

					new File(geckoDriverloc).setExecutable(true);
				}
				System.setProperty(GeckoDriverService.GECKO_DRIVER_EXE_PROPERTY, geckoDriverloc);
				DesiredCapabilities geckocap = getFireFoxDesiredCapabilities();
				try {
					if (isSeleniumGridEnabled) {
						log.info(String.format("Grid is enabled [%s]", getSeleniumGridHubUrl()));
						RemoteWebDriver remoteWebDriver = new RemoteWebDriver(getSeleniumGridHubUrl(), geckocap);
						remoteWebDriver.setFileDetector(new LocalFileDetector());
						return remoteWebDriver;
					} else {
						return new FirefoxDriver(geckocap);
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					log.warn("Error creating driver", e);
					throw new RuntimeException("Failed to get the driver", e);
				}
			case CHROME:
				String chromeDriverLocation;
				if (System.getProperty("os.name").contains("Windows")) {
					log.info("Client Machine is windows");
					chromeDriverLocation = FileUtils.copyRecourseToTemporaryFolder("chromedriver.exe");
				} else {
					log.info("Machine is not windows");
					chromeDriverLocation = FileUtils.copyRecourseToTemporaryFolder("chromedriver");

					new File(chromeDriverLocation).setExecutable(true);
				}
				System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, chromeDriverLocation);
				DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
				ChromeOptions option = new ChromeOptions();
				Map<String,Object> pref = new HashMap<>();
				pref.put("download.prompt_for_download", false);
				pref.put("download.default_directory", getDownloadDirectory());
				option.setExperimentalOption("prefs", pref);
				option.addArguments("--start-maximized");
				chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, option);
				try {
					if (isSeleniumGridEnabled) {
						log.info(String.format("Grid is enabled [%s]", getSeleniumGridHubUrl()));
						RemoteWebDriver remoteWebDriver = new RemoteWebDriver(getSeleniumGridHubUrl(), chromeCapabilities);
						remoteWebDriver.setFileDetector(new LocalFileDetector());
						return remoteWebDriver;
					} else {
						return new ChromeDriver(chromeCapabilities);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.warn("Error creating driver", e);
					throw new RuntimeException("Failed to get the driver", e);
				}
			case SAUCELABS:
				DesiredCapabilities caps = DesiredCapabilities.chrome();
				caps.setCapability("platform", "Windows 8.1");
				caps.setCapability("version", "latest");
				caps.setCapability("browserName", "chrome");

				try {
					return new RemoteWebDriver(new URL(URL), caps);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}

			default:
				throw new UnsupportedOperationException("Attempt to start invalid browser " + browser);
		}
	}


	private static DesiredCapabilities getFireFoxDesiredCapabilities()
	{
		String neverAskSaveToDiskAndOpenFileValues = "application/octet-stream, application/x-zip-compressed, " +
				"application/zip-compressed, application/zip, multipart/x-zip, application/x-compressed, " +
				"application/msword, text/plain, image/gif, image/png, application/pdf, application/excel, " +
				"application/vnd.ms-excel, application/x-excel, application/x-msexcel, text/csv";
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("app.update.enabled", false);
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.dir", getDownloadDirectory());
		profile.setPreference("browser.helperApps.alwaysAsk.force", false);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.panel.shown", true);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk", neverAskSaveToDiskAndOpenFileValues);
		profile.setPreference("browser.helperApps.neverAsk.openFile", neverAskSaveToDiskAndOpenFileValues);
		DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
		firefoxCapabilities.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR, ElementScrollBehavior.BOTTOM);
		firefoxCapabilities.setCapability(FirefoxDriver.PROFILE, profile);
		return firefoxCapabilities;
	}

	public static String getDownloadDirectory() {
		return System.getProperty("user.dir") + File.separator + "target";
	}


	private static URL getSeleniumGridHubUrl() {
		try {
			return new URL("http://" + PropertyUtils.getSeleniumGridHubUrl() + "/wd/hub");
		} catch (MalformedURLException e) {
			throw new RuntimeException("Selenium grid hub URL is not valid", e);
		}
	}
}