package test.automation.framework.ui.utils;

import java.util.Map;

import javax.ws.rs.core.NewCookie;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import test.automation.framework.utils.PropertyUtils;

public class WebUtils
{

	private static final Logger log = Logger.getLogger(WebUtils.class);

	/**
	 * Method to fill the text on webelement
	 * @param element
	 * @param value
	 */
	public static void fill(WebElement element, String value){
		if(!isElementDisplayed(element))
		{
			throw new ElementNotVisibleException(element+" is not visible");
		}

		element.clear();

		element.sendKeys(value);
	}

	
	/**
	 * Method to select value from drop down
	 * @param element
	 * @param value
	 */
	public static void selectDropDownValue(WebElement element, String value)
	{
		if(!isElementDisplayed(element))
		{
			throw new ElementNotVisibleException(element+" is not visible");
		}
		Select select = new Select(element);
		long count = select.getOptions().stream().map(WebElement::getText).filter(s-> s.equals(value)).count();
		if(count!=1)
		{
			throw new AssertionError(String.format("unable to select the '%s' value, there are %s matches",value,count));
		}
		if(value.equals(StringUtils.EMPTY))
		{
			select.selectByVisibleText(value);
		}
		else
		{
			element.sendKeys(value);
			element.sendKeys(Keys.ENTER);
		}
	}


	/**
	 * Method to check whether the element is present or not 
	 * @param element
	 * @return
	 */
	public static boolean isElementDisplayed(WebElement element)
	{
		try
		{
			return element.isDisplayed();
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
		catch(StaleElementReferenceException e)
		{
			return false;
		}
	}


	/**
	 * Method to check whether the element is present or not using timeout
	 * @param driver
	 * @param element
	 * @param timeout
	 * @return
	 */
	public static boolean isElementDisplayed(WebDriver driver,WebElement element, long timeout)
	{
		try
		{
			new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(element));
			return true;
		}
		catch(TimeoutException e)
		{
			return false;
		}
	}



	/**
	 * Wait for element to be present 
	 * @param driver
	 * @param element
	 * @param timeout
	 */
	public static void waitForElementToBeDisplayed(WebDriver driver,WebElement element, long timeout)
	{
		try
		{
			new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(element));

		}
		catch(TimeoutException e)
		{
			throw new ElementNotVisibleException("Timeout"+element+" is not visible/present.");
		}
	}

	/**
	 * Wait for element to be present
	 * @param driver
	 * @param element
	 * @param timeout
	 */
	public static void waitForElementToBeClickable(WebDriver driver,WebElement element, long timeout)
	{
		try
		{
			new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));

		}
		catch(TimeoutException e)
		{
			throw new ElementNotVisibleException("Timeout"+element+" is not visible/present.");
		}
	}

	/**
	 * Wait for element to be present
	 * @param driver
	 * @param element
	 * @param timeout
	 */
	public static void waitForPresenceOfElement(WebDriver driver,WebElement element, long timeout)
	{
		try
		{
			new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfNestedElementLocatedBy(element,
					By.tagName("option")));

		}
		catch(TimeoutException e)
		{
			throw new ElementNotVisibleException("Timeout"+element+" is not visible/present.");
		}
	}

	/**
	 * Wait for element to not be present
	 * @param driver
	 * @param element
	 * @param timeout
	 */
	public static void waitForInvisibillityOfElement(WebDriver driver,WebElement element, long timeout)
	{
		try
		{
			new WebDriverWait(driver, timeout).until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));

		}
		catch(TimeoutException e)
		{
			throw new ElementNotVisibleException("Timeout"+element+" is not visible/present.");
		}
	}

	/**
	 * Wait for element to be present
	 * @param driver
	 * @param element
	 * @param timeout
	 */
	public static void waitForElementLoaded(WebDriver driver,WebElement element, long timeout)
	{
		try
		{
			new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOfAllElements(element));

		}
		catch(TimeoutException e)
		{
			throw new ElementNotVisibleException("Timeout"+element+" is not visible/present.");
		}
	}







	/**
	 * Method to send keys event
	 * @param driver
	 * @param key
	 */
	public static void sendKeys(WebDriver driver, Keys key)
	{
		new Actions(driver).sendKeys(Keys.chord(key)).perform();
	}


	/**
	 * Method to wait for page load
	 * @param driver
	 */
	public static void waitForPageLoad(WebDriver driver)
	{
		new WebDriverWait(driver, 120).until(
				webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
	}


	/**
	 * 
	 * @param elem
	 * @return
	 */
	public static String getSelectedValueFromSelection(WebElement elem)
	{
		if(!isElementDisplayed(elem))
		{
			throw new ElementNotVisibleException(elem+" is not visible");
		}
		Select select = new Select(elem);
		WebElement option = select.getFirstSelectedOption();
		return option.getText();
	}


	/**
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementPresent(WebElement element)
	{
		if(element.isDisplayed())
		{
			return true;
		}
		return false;
	}


	/**
	 * Method to click on element after waiting for specified time
	 * @param elem
	 * @param timeout
	 */
	public static void clickWithWaitForElement(WebDriver wd,WebElement elem, long timeout)
	{
		WebUtils.waitForElementToBeDisplayed(wd, elem, timeout);
		elem.click();
	}


	/**
	 * Method to click on element with default waiting time
	 * @param elem
	 */
	public static void clickWithWaitForElement(WebDriver wd,WebElement elem)
	{
		WebUtils.waitForElementToBeDisplayed(wd, elem, PropertyUtils.getDefaultTimeOutForElement());
		elem.click();
	}


	/**
	 * Method to verify text value
	 * @param elem
	 */
	public String getTextValue(WebDriver wd,WebElement elem)
	{
		WebUtils.waitForElementToBeDisplayed(wd, elem, PropertyUtils.getDefaultTimeOutForElement());
		return elem.getText();
	}

	
	/**
	 * Method to hover on root element and then click child element
	 * @param rootElement
	 * @param childElement
	 */
	public static void hoverAndClick(WebDriver wd, WebElement rootElement, WebElement childElement)
	{
		WebUtils.waitForElementToBeDisplayed(wd, rootElement, PropertyUtils.getDefaultTimeOutForElement());

		Actions action = new Actions(wd);

		action.moveToElement(rootElement).perform();

		action.moveToElement(childElement);

		action.click();

		action.perform();
	}


	/**
	 * Method to add cookies to webdriver
	 * @param cookies
	 */
	public static void setDriverCookies(WebDriver wd,Map<String,NewCookie> cookies)
	{
		for (NewCookie c: cookies.values()) {
			log.info("cooke name - "+c.toCookie().getName()+" value -"+c.toCookie().getValue());
			Cookie cookie = new Cookie(c.toCookie().getName(), c.toCookie().getValue());
			wd.manage().addCookie(cookie);

		}
	}
}
