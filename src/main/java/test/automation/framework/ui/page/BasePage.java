package test.automation.framework.ui.page;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import test.automation.framework.utils.PropertyUtils;

public abstract class BasePage<T extends BasePage> 
{
	protected WebDriver wd;
	String appsUrl = PropertyUtils.getsAppUrl();
	protected Logger log = Logger.getLogger(getClass());
	protected List<WebElement> errorElements = new ArrayList<WebElement>();

	public abstract String getURL();


	public BasePage(WebDriver driver)
	{
		this(driver,false);
		clearErroredElementList();
	}

	public BasePage(WebDriver driver , boolean waitForLoad)
	{
		wd = driver;
		PageFactory.initElements(wd, this);

	}

	public void acceptAlertIfPresent(WebDriver wd)
	{
		try
		{

			wd.switchTo().alert().accept();
		}
		catch (NoAlertPresentException ignored ) {
			// TODO: handle exception
		}
	}

	/**
	 * 
	 * @return
	 */
	public T navigateTo()
	{
		log.info(String.format("Navigating to url - '%s'",getURL()));
		String url = PropertyUtils.getsAppUrl()+getURL();
		if(wd.getCurrentUrl().equals(url))
		{
			wd.navigate().refresh();
		}
		wd.get(url);
		acceptAlertIfPresent(wd);
		return (T)this;
	}
	
	/**
	 * 
	 * @param elem
	 */
	protected void setErroredElementList(WebElement elem)
	{
		errorElements.add(elem);
	}
	
	/**
	 * 
	 */
	protected void clearErroredElementList()
	{
		errorElements.clear();
	}
	
	public List<WebElement> getErroredElements()
	{
		return this.errorElements;
	}
}
