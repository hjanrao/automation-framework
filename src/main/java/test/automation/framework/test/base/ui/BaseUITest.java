package test.automation.framework.test.base.ui;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import test.automation.framework.ui.utils.WebDriverManager;
import test.automation.framework.test.base.BaseTest;

public class BaseUITest extends BaseTest
{
	protected WebDriver wd = null;
	protected Logger log = Logger.getLogger(getClass());

	@BeforeClass(alwaysRun=true)
	public void initializeDriver()
	{
		log.info("Start initializing the driver");
		try
		{
			wd = WebDriverManager.getWebDriver();
		}
		catch(Exception e)
		{
			Assert.fail("Error creating driver", e);
		}

		log.info("Finish initializing the driver");
	}

	@AfterClass(alwaysRun=true)
	public void destroyDriver()
	{
		try
		{

			log.info("Destroying the driver");
			wd.quit();
		}
		catch (UnhandledAlertException e) {
			wd.switchTo().alert().accept();
		}
	}

	/**
	 * method to take screen shot
	 * @param testName
	 * @throws IOException
	 */
	public void takeScreenShot(String testName)
	{
		if(Objects.isNull(wd))
		{
			log.warn("WebDriver is null, unable to save screenshot");
			return;
		}
		try
		{
			TakesScreenshot shot = (TakesScreenshot)wd;
			File file = shot.getScreenshotAs(OutputType.FILE);
			String fileName= String.format("snapshot%s%s%s.png",testName,LocalDateTime.now().
					format(DateTimeFormatter.ofPattern("ddmmyyyy")),RandomStringUtils.randomAlphabetic(3));
			Path path = Paths.get("target/screenshot",testName,fileName);
			Files.createDirectories(path.getParent());
			Files.copy(file.toPath(), path,StandardCopyOption.REPLACE_EXISTING);	
		}
		catch(IOException e)
		{
			log.error("Screenshot saving failed", e);
		}
	}
}
