package test.automation.framework.test.base.api;

import org.testng.annotations.BeforeClass;

import test.automation.framework.test.base.BaseTest;
import test.automation.framework.utils.PropertyUtils;

public class BaseTestApi extends BaseTest
{
	protected static final String SERVER = PropertyUtils.getServer();
	protected static final int  PORT = PropertyUtils.getPort();
	protected static final String PROTOCOL = PropertyUtils.getProtocol();
	
	
	
	@BeforeClass
	public void beforeBaseTestApi()
	{
		log.info("Inside before class 'BaseTestApi'");
	}
	

}
