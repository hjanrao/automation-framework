package test.automation.framework.api.connection;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;

public class ServerConnection
{


	protected Client client;
	protected String rootURL;
	protected Map<String, String> customHeader = new HashMap<String,String>();
	protected Map<String,NewCookie> cookies = null;
	private Logger logger = Logger.getLogger(ServerConnection.class);
	

	public ServerConnection() {
	}

	private WebTarget createWebTarget(String path) {
		UriBuilder uriBuilder = UriBuilder.fromUri(this.rootURL);
		if (path.contains("?")) {
			String basePath = path.split("\\?")[0];
			String query = path.split("\\?")[1];
			String[] params = query.split("&");
			uriBuilder.path(basePath);
			String[] var6 = params;
			int var7 = params.length;

			for(int var8 = 0; var8 < var7; ++var8) {
				String param = var6[var8];
				String key = param.substring(0, param.indexOf("="));
				String value = param.substring(param.indexOf("=") + 1);
				uriBuilder.queryParam(key, new Object[]{value.replace(" ", "%20")});
			}
		} else {
			uriBuilder.path(path);
		}

		URI uri = uriBuilder.build(new Object[0]);
		WebTarget target = this.client.target(uri);
		return target;
	}

	private Builder createRequest(String path, MediaType Type) {
		Builder builder = this.createWebTarget(path).request(new MediaType[]{Type});

		if (!this.customHeader.isEmpty()) {
			builder = this.addCustomHeader(builder);
		}
		if(this.cookies!=null)
		{
			builder = this.addCookie(builder);

		}
		
		return builder;
		
	}

	private Builder addCookie(Builder builder)
	{
		logger.info("Adding cookie to the request");
		for (NewCookie c: cookies.values()) {
			System.out.println("cooke name - "+c.toCookie().getName()+" value -"+c.toCookie().getValue());
			builder=builder.cookie(c.toCookie().getName(), c.toCookie().getValue());
		    
		}
		
		return builder;
	}

	private Builder createRequest(String path) {
		return this.createRequest(path, MediaType.APPLICATION_JSON_TYPE);
	}

	public <T> T getRequest(String path, Class<T> responseType) {
		T response = this.createRequest(path).get(responseType);
		return response;
	}

	protected Response getRequest(String path) {
		Response response = (Response)this.getRequest(path, Response.class);
		return response;
	}

	public <T> Response postRequest(String path, T request, MediaType mediaType) {
		Entity<T> entity = Entity.entity(request, mediaType);
		Response response = this.createRequest(path).post(entity);
		return response;
	}

	public <T> Response postRequest(String path, T request) {
		Entity<T> entity = Entity.entity(request, MediaType.APPLICATION_JSON_TYPE);
		Response response = this.createRequest(path).post(entity);
		return response;
	}

	public  Response postRequest(String path, JsonObject jsonIP) {
		Response response = this.createRequest(path).post(Entity.entity(jsonIP.toString(), "application/json"));
		return response;
	}


	public  Response patchtRequest(String path, JsonArray jsonIP) {
		Response response = this.createRequest(path).post(Entity.entity(jsonIP.toString(), "application/json-patch+json"));
		return response;
	}


	private Builder addCustomHeader(Builder builder) {
		Entry entry;
		if (!this.customHeader.isEmpty()) {
			for(Iterator var2 = this.customHeader.entrySet().iterator(); var2.hasNext(); builder = builder.header((String)entry.getKey(), entry.getValue())) {
				entry = (Entry)var2.next();
			}
		}

		return builder;
	}


	
	/*protected Response putRequestWithAttachment(String path, JSONObject request, List<Attachment> attachments) throws IOException {
        FormDataMultiPart multiPart = new FormDataMultiPart();
        Throwable var5 = null;

        try {
            FormDataBodyPart entryPart = new FormDataBodyPart("recordInstance", request, MediaType.APPLICATION_JSON_TYPE);
            multiPart.bodyPart(entryPart);
            Iterator var7 = attachments.iterator();

            while(var7.hasNext()) {
                Attachment att = (Attachment)var7.next();
                FileDataBodyPart attachmentPart = new FileDataBodyPart("" + att.getAttachmentFieldId(), new File(att.getFileName()), MediaType.APPLICATION_OCTET_STREAM_TYPE);
                multiPart.bodyPart(attachmentPart);
            }

            Entity<FormDataMultiPart> entity = Entity.entity(multiPart, "multipart/form-data");
            Response response = this.createRequest(path).put(entity);
            Response var22 = response;
            return var22;
        } catch (Throwable var18) {
            var5 = var18;
            throw var18;
        } finally {
            if (multiPart != null) {
                if (var5 != null) {
                    try {
                        multiPart.close();
                    } catch (Throwable var17) {
                        var5.addSuppressed(var17);
                    }
                } else {
                    multiPart.close();
                }
            }

        }
    }*/
	/**
	 * 
	 * @param url
	 * @param querpParamIdValuePair
	 * @return
	 * @throws MalformedURLException
	 */
	protected String resolveTemplates(String url,Map<String,String> querpParamIdValuePair) throws MalformedURLException {
		Map<String, Object> templateValues = new HashMap<String, Object>();
		for (Map.Entry<String, String> entry : querpParamIdValuePair.entrySet())
		{
			templateValues.put(entry.getKey(),entry.getValue());
		}
		WebTarget webTarget = client.target(url).resolveTemplates(
				templateValues);
		return webTarget.getUriBuilder().toString();

	}
	
	
	/*protected ResponseMessage setStatusMessage(Response res)
	{
		ResponseMessage status = new ResponseMessage();
		status.setErrorCode(res.getStatus());
	}*/
}
