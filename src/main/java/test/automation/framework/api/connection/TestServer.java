package test.automation.framework.api.connection;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.*;

import com.sun.jersey.core.util.MultivaluedMapImpl;

import test.automation.framework.api.services.AbstractService;
import test.automation.framework.extranetURLs;


import org.apache.log4j.Logger;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

public class TestServer extends ServerConnection
{
	private String serverName = null;
	private String protocol = null;
	private int port = 0;

	private static final Logger logger = Logger.getLogger(TestServer.class);


	/**
	 * 
	 * @param serverName
	 * @param protocol
	 * @param httpPort
	 */
	public TestServer(String serverName, String protocol, int httpPort) {

		this.serverName = serverName;
		this.protocol = protocol;
		this.port = httpPort;
		rootURL = this.protocol + "://" + this.serverName + (port == 0 ? "" : (":" + port));
		setCustomHeader(new HashMap<String, String>());
		client = ClientBuilder.newBuilder().register(JacksonFeature.class).register(MultiPartFeature.class).build();
	}


	private void login(String username,String password, boolean isAdmin)
	{
		MultivaluedMap<String,String> loginData = new MultivaluedMapImpl();
		String loginUrl = null;
		if(isAdmin)
		{

			loginUrl = extranetURLs.ADMIN_LOGIN;
		}
		else
		{
			loginUrl= extranetURLs.SUPPLIER_LOGIN;
		}

		loginData.add("username",username);
		loginData.add("password",password);
		Response res = this.postRequest(loginUrl,loginData, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		logger.info("Status after login is - %s");
		setCookies(res.getCookies());

	}


	/**
	 * 
	 */
	public void loginAdmin()
	{
		String username = System.getenv("admin");
		String password = System.getenv("password");
		login(username,password,true);

	}


	/**
	 * 
	 * @param username
	 * @param password
	 */
	public void loginSupplier(String username,String password)
	{
		login(username,password,false);

	}



	/**
	 * Method to add custom header
	 * @param header
	 */
	public void setCustomHeader(Map<String, String> header) {
		customHeader = header;
	}

	
	/**
	 * 
	 * @param cookies
	 */
	public void setCookies(Map<String,NewCookie> cookies)
	{
		this.cookies = cookies;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Map<String,NewCookie> getCookies()
	{
		return this.cookies;
	}


	/**
	 * @param service
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException

	 */
	@SuppressWarnings("rawtypes")
	public <T extends AbstractService> T getService(Class<T> service) throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{

		if(!AbstractService.mapHolder.containsKey(service)){

            T obj = service.newInstance();

            AbstractService.mapHolder.put(service, obj);
        }

        return (T)AbstractService.mapHolder.get(service);

    }

}
