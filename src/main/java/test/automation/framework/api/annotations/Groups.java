package test.automation.framework.api.annotations;

public class Groups 
{
	 /**
     * Sanity level tests
     */
    public static final String CATEGORY_SANITY = "Sanity";
    
    /**
     * Confidence level tests
     */
    public static final String CATEGORY_CONFIDENCE = "confidence";
    
    
    /**
     * API Test
     */
    public static final String CATEGORY_API = "API";
    
    
    /**
     * Build UI
     */
    public static final String CATEGORY_UI = "UI";
}