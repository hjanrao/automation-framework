package test.automation.framework.api.constants;
import test.automation.framework.api.services.OrderManagementService;

public enum ManagedServices {

	ORDER_MANAGEMENT(OrderManagementService.class);

	private final Class<?> type;

	private ManagedServices(Class<?> type) {
		this.type = type;
	}

	public Class<?> getType(){
		return this.type;
	}
}