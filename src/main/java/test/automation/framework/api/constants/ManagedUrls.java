package test.automation.framework.api.constants;

public enum ManagedUrls 
{
ORDER_MANAGEMENT("/order_management.php");

private String url;

private ManagedUrls(String url) {
	this.url=url;
}

public String getUrl()
{
	return url;
}

}


