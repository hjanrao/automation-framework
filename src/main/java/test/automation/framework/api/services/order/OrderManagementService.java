package test.automation.framework.api.services.order;

import javax.ws.rs.core.Response;

import test.automation.framework.api.constants.ManagedUrls;
import test.automation.framework.api.services.AbstractService;

public class OrderManagementService extends AbstractService<OrderManagementService>
{

	private String orderUrl = ManagedUrls.ORDER_MANAGEMENT.getUrl();;
	private String orderManagementUrl="/order_management.php";



	/**
	 * 
	 * @return
	 */
	public Response getOrderManagement()
	{
		String url = orderManagementUrl;
		Response res = getRequest(orderUrl);
		return res;
	}
}
