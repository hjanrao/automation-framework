package test.automation.framework.api.services;

import com.sun.jersey.core.util.MultivaluedMapImpl;
import test.automation.framework.api.connection.TestServer;
import test.automation.framework.extranetURLs;
import test.automation.framework.utils.PropertyUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

public class SwitchSupplierService extends TestServer {

    private static SwitchSupplierService switchSupplier =  null;

    /**
     * @param serverName
     * @param protocol
     * @param httpPort
     */
    private SwitchSupplierService(String serverName, String protocol, int httpPort) {
        super(serverName, protocol, httpPort);
    }

    private void SwitchSupplierService(){ }

    public static SwitchSupplierService getSwitchSupplierInstance(){
        if(switchSupplier == null) {
            switchSupplier = new SwitchSupplierService(PropertyUtils.getServer(),
                    PropertyUtils.getProtocol(),
                    PropertyUtils.getPort());
        }
        return switchSupplier;
    }


    public Response postChangeSupplier(int supplierId)
    {
        MultivaluedMap<String,String> supplierDetails = new MultivaluedMapImpl();
        supplierDetails.add("switchsupplier", String.valueOf(supplierId));
        supplierDetails.add("changeSupplier","Go");

        Response res = this.postRequest(extranetURLs.LANDING_PAGE,supplierDetails,
                MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        setCookies(res.getCookies());
        return res;
    }
}
