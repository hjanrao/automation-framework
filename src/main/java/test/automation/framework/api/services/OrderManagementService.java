package test.automation.framework.api.services;

import test.automation.framework.api.connection.TestServer;
import test.automation.framework.extranetURLs;
import test.automation.framework.utils.PropertyUtils;

import javax.ws.rs.core.Response;

public class OrderManagementService extends TestServer {

    private static OrderManagementService orderManegement =  null;

    /**
     * @param serverName
     * @param protocol
     * @param httpPort
     */
    private OrderManagementService(String serverName, String protocol, int httpPort) {
        super(serverName, protocol, httpPort);
    }

    private void OrderManagement(){ }

    public static OrderManagementService getOrderManagementInstance(){
        if(orderManegement == null) {
            orderManegement = new OrderManagementService(PropertyUtils.getServer(),
                    PropertyUtils.getProtocol(),
                    PropertyUtils.getPort());
        }
        return orderManegement;
    }

    public Response getOrderManagementService()
    {
        Response res = getRequest(extranetURLs.ORDER_MANAGEMENT);
        return res;
    }

}
