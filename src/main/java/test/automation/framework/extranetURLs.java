package test.automation.framework;

public interface extranetURLs {
    String ADMIN_LOGIN = "/admin";
    String SUPPLIER_LOGIN="/v/login/index";
    String ORDER_MANAGEMENT="/order_management.php";
    String LANDING_PAGE = "/v/landing/index";
}
